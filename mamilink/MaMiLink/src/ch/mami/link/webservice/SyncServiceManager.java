/*
 * 
 */
package ch.mami.link.webservice;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ch.mami.link.controller.BaseManager;
import ch.mami.link.model.Comment;
import ch.mami.link.model.Credentials;
import ch.mami.link.model.Follow;
import ch.mami.link.model.Link;
import ch.mami.link.model.Linklist;
import ch.mami.link.model.User;
import ch.mami.link.webservice.SyncManager.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class SyncServiceManager.
 */
@Path("/sync")
public class SyncServiceManager extends BaseManager {

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/users")
	public Response getUsers() {
		System.out.println("get users");
		List<User> users = entityManager.createQuery("select u from users u",
				User.class).getResultList();

		Response response = Response.status(200).entity(users).build();

		return response;
	}

	/**
	 * Sets the users.
	 *
	 * @param users
	 *            the users
	 * @return the response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/users")
	public Response setUsers(List<User> users) {
		SyncManager syncManager = new SyncManager(entityManager, utx);
		System.out.println("put users");
		syncManager.syncUsers(users);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		return Response.noContent().build();
	}

	/**
	 * Gets the credentials.
	 *
	 * @return the credentials
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/creds")
	public Response getCredentials() {
		System.out.println("get users");
		List<Credentials> credentials = entityManager.createQuery(
				"select c from credentials c", Credentials.class)
				.getResultList();

		Response response = Response.status(200).entity(credentials).build();

		return response;
	}

	/**
	 * Sets the credentials.
	 *
	 * @param credentials
	 *            the credentials
	 * @return the response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/creds")
	public Response setCredentials(List<Credentials> credentials) {
		SyncManager syncManager = new SyncManager(entityManager, utx);
		System.out.println("put credentials");
		syncManager.syncCredentials(credentials);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		return Response.noContent().build();
	}

	/**
	 * Gets the lists.
	 *
	 * @return the lists
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/lists")
	public Response getLists() {
		System.out.println("get lists");
		List<Linklist> linklists = entityManager.createQuery(
				"select l from linklists l", Linklist.class).getResultList();

		Response response = Response.status(200).entity(linklists).build();

		return response;
	}

	/**
	 * Sets the lists.
	 *
	 * @param linklists
	 *            the linklists
	 * @return the response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/lists")
	public Response setLists(List<Linklist> linklists) {
		SyncManager syncManager = new SyncManager(entityManager, utx);
		System.out.println("put lists");
		syncManager.syncLinklists(linklists);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		return Response.noContent().build();
	}

	/**
	 * Gets the links.
	 *
	 * @return the links
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/links")
	public Response getLinks() {
		System.out.println("get links");
		List<Link> links = entityManager.createQuery("select l from links l",
				Link.class).getResultList();

		Response response = Response.status(200).entity(links).build();

		return response;
	}

	/**
	 * Sets the links.
	 *
	 * @param links
	 *            the links
	 * @return the response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/links")
	public Response setLinks(List<Link> links) {
		SyncManager syncManager = new SyncManager(entityManager, utx);
		System.out.println("put links");
		syncManager.syncLinks(links);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		return Response.noContent().build();
	}

	/**
	 * Gets the list comments.
	 *
	 * @return the list comments
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/comments")
	public Response getListComments() {
		System.out.println("get comments");
		List<Comment> comments = entityManager.createQuery(
				"select c from comments c", Comment.class).getResultList();

		Response response = Response.status(200).entity(comments).build();

		return response;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments
	 *            the comments
	 * @return the response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/comments")
	public Response setComments(List<Comment> comments) {
		SyncManager syncManager = new SyncManager(entityManager, utx);
		System.out.println("put comments");
		syncManager.syncComments(comments);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		return Response.noContent().build();
	}

	/**
	 * Gets the list follows.
	 *
	 * @return the list follows.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/follows")
	public Response getFollows() {
		System.out.println("get follows");
		List<Follow> follows = entityManager.createQuery(
				"select f from follows f", Follow.class).getResultList();

		Response response = Response.status(200).entity(follows).build();

		return response;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments
	 *            the comments
	 * @return the response
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/follows")
	public Response setFollows(List<Follow> follows) {
		SyncManager syncManager = new SyncManager(entityManager, utx);
		System.out.println("put follows");
		syncManager.syncFollows(follows);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		return Response.noContent().build();
	}
}
