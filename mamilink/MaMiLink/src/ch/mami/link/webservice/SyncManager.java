/*
 * 
 */
package ch.mami.link.webservice;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import ch.mami.link.controller.BaseManager;
import ch.mami.link.model.Comment;
import ch.mami.link.model.Credentials;
import ch.mami.link.model.Follow;
import ch.mami.link.model.Link;
import ch.mami.link.model.Linklist;
import ch.mami.link.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class SyncManager.
 */
public class SyncManager extends BaseManager {

	/**
	 * Instantiates a new sync manager.
	 *
	 * @param entityManager
	 *            the entity manager
	 */
	public SyncManager(EntityManager entityManager, UserTransaction utx) {
		super.entityManager = entityManager;
		super.utx = utx;
		// super.
		log = new ArrayList<Log>();
	}

	/**
	 * Sync users.
	 *
	 * @param remotes
	 *            the remotes
	 * @return the list
	 */
	public List<User> syncUsers(List<User> remotes) {
		List<User> locals = entityManager.createQuery("select u from users u",
				User.class).getResultList();

		for (User remote : remotes) {
			User local;
			if (locals.contains(remote)) {
				// exists already
				local = locals.get(locals.indexOf(remote));
				log.add(new Log("user - sync - " + local.getUuid().toString(),
						local.getUsername()));
				
				// sync just if remote is newer than local
				if (local.getLastChange().compareTo(remote.getLastChange()) > 0) {
					continue;
				}
			} else {
				// create new
				local = new User();
				log.add(new Log("user - add - " + remote.getUuid().toString(),
						remote.getUsername()));
			}
			if (checkUserConstraints(remote)) {
				copyEntity(remote, local);
				super.save(local);
				locals.remove(local);
			} else {
				System.out.println("unique constraint violation ");
				log.add(new Log("unique constraint violation ", "user ".concat(
						remote.getUsername()).concat(" is already in use!")));
			}
		}

		return locals;
	}
	
	/**
	 * Sync creds.
	 *
	 * @param remotes
	 *            the remotes
	 * @return the list
	 */
	public List<Credentials> syncCredentials(List<Credentials> remotes) {
		List<Credentials> locals = entityManager.createQuery("select c from credentials c",
				Credentials.class).getResultList();

		for (Credentials remote : remotes) {
			Credentials local;
			if (locals.contains(remote)) {
				// exists already
				local = locals.get(locals.indexOf(remote));
				log.add(new Log("creds - sync - " + local.getUuid().toString(),
						local.getUsername()));
				
				// sync just if remote is newer than local
				if (local.getLastChange().compareTo(remote.getLastChange()) > 0) {
					continue;
				}
			} else {
				// create new
				local = new Credentials();
				log.add(new Log("creds - add - " + remote.getUuid().toString(),
						remote.getUsername()));
			}
			copyEntity(remote, local);
			super.save(local);
			locals.remove(local);
		}

		return locals;
	}

	/**
	 * Checks if already a user with same username and other uuid exists.
	 *
	 * @param remote
	 *            the remote user
	 * @return boolean
	 */
	private boolean checkUserConstraints(User remote) {
		return (entityManager
				.createQuery(
						"select count(u) from users u where username = :username and uuid <> :uuid",
						Long.class)
				.setParameter("username", remote.getUsername())
				.setParameter("uuid", remote.getUuid()).getSingleResult() == 0);
	}

	/**
	 * Sync linklists.
	 *
	 * @param remotes
	 *            the remotes
	 * @return the list
	 */
	public List<Linklist> syncLinklists(List<Linklist> remotes) {
		List<Linklist> locals = entityManager.createQuery(
				"select l from linklists l", Linklist.class).getResultList();

		for (Linklist remote : remotes) {
			Linklist local;
			if (locals.contains(remote)) {
				// exists already
				local = locals.get(locals.indexOf(remote));
				log.add(new Log("list - sync - " + local.getUuid().toString(),
						local.getTitle()));
				
				// sync just if remote is newer than local
				if (local.getLastChange().compareTo(remote.getLastChange()) > 0) {
					continue;
				}
			} else {
				// create new
				System.out.println("new");
				local = new Linklist();
				log.add(new Log("list - add - " + remote.getUuid().toString(),
						remote.getTitle()));
			}
			copyEntity(remote, local);
			super.save(local);
			locals.remove(local);
		}

		return locals;
	}

	/**
	 * Sync links.
	 *
	 * @param remotes
	 *            the remotes
	 * @return the list
	 */
	public List<Link> syncLinks(List<Link> remotes) {
		List<Link> locals = entityManager.createQuery("select l from links l",
				Link.class).getResultList();

		for (Link remote : remotes) {
			Link local;
			if (locals.contains(remote)) {
				// exists already
				local = locals.get(locals.indexOf(remote));
				log.add(new Log("link - sync - " + local.getUuid().toString(),
						local.getTitle()));
				
				// sync just if remote is newer than local
				if (local.getLastChange().compareTo(remote.getLastChange()) > 0) {
					continue;
				}
			} else {
				// create new
				local = new Link();
				log.add(new Log("link - add - " + remote.getUuid().toString(),
						remote.getTitle()));
			}
			copyEntity(remote, local);
			super.save(local);
			locals.remove(local);
		}

		return locals;
	}

	/**
	 * Sync comments.
	 *
	 * @param remotes
	 *            the remotes
	 * @return the list
	 */
	public List<Comment> syncComments(List<Comment> remotes) {
		List<Comment> locals = entityManager.createQuery(
				"select c from comments c", Comment.class).getResultList();

		for (Comment remote : remotes) {
			Comment local;
			if (locals.contains(remote)) {
				// exists already
				local = locals.get(locals.indexOf(remote));
				log.add(new Log("comment - sync - "
						+ local.getUuid().toString(), local.getTitle()));
				
				// sync just if remote is newer than local
				if (local.getLastChange().compareTo(remote.getLastChange()) > 0) {
					continue;
				}
			} else {
				// create new
				local = new Comment();
				log.add(new Log(
						"comment - add - " + remote.getUuid().toString(), remote
								.getTitle()));
			}
			copyEntity(remote, local);
			super.save(local);
			locals.remove(local);
		}

		return locals;
	}
	
	/**
	 * Sync follows.
	 *
	 * @param remotes
	 *            the remotes
	 * @return the list
	 */
	public List<Follow> syncFollows(List<Follow> remotes) {
		List<Follow> locals = entityManager.createQuery(
				"select f from follows f", Follow.class).getResultList();

		for (Follow remote : remotes) {
			Follow local;
			if (locals.contains(remote)) {
				// exists already
				local = locals.get(locals.indexOf(remote));
				log.add(new Log("follow - sync - "
						+ local.getUuid().toString(), ""));
				
				// sync just if remote is newer than local
				if (local.getLastChange().compareTo(remote.getLastChange()) > 0) {
					continue;
				}
			} else {
				// create new
				local = new Follow();
				log.add(new Log(
						"comment - add - " + remote.getUuid().toString(), ""));
			}
			copyEntity(remote, local);
			super.save(local);
			locals.remove(local);
		}

		return locals;
	}

	private void copyEntity(User from, User to) {
		if ((from != null) && (to != null)) {
			to.setUsername(from.getUsername());
			to.setName(from.getName());
			to.setSurname(from.getSurname());
			to.setEmail(from.getEmail());
			to.setUuid(from.getUuid());
			to.setAdmin(from.getAdmin());
		}
	}
	
	private void copyEntity(Credentials from, Credentials to) {
		if ((from != null) && (to != null)) {
			to.setUsername(from.getUsername());
			to.setPassword(from.getPassword());
			to.setUser(getUserByUuid(from.getUser().getUuid()));
			to.setUuid(from.getUuid());
		}
	}

	private void copyEntity(Linklist from, Linklist to) {
		if ((from != null) && (to != null)) {
			to.setTitle(from.getTitle());
			to.setOwner(getUserByUuid(from.getOwner().getUuid()));
			to.setPrivat(from.getPrivat());
			to.setPosition(from.getPosition());
			to.setUuid(from.getUuid());
		}
	}

	private void copyEntity(Link from, Link to) {
		if ((from != null) && (to != null)) {
			to.setTitle(from.getTitle());
			to.setList(getLinklistByUuid(from.getList().getUuid()));
			to.setUrl(from.getUrl());
			to.setPosition(from.getPosition());
			to.setUuid(from.getUuid());
		}
	}

	private void copyEntity(Comment from, Comment to) {
		if ((from != null) && (to != null)) {
			to.setTitle(from.getTitle());
			to.setMessage(from.getMessage());
			if (from.getList() != null) {
				to.setList(getLinklistByUuid(from.getList().getUuid()));
			}
			if (from.getLink() != null) {
				to.setLink(getLinkByUuid(from.getLink().getUuid()));
			}
			to.setOwner(getUserByUuid(from.getOwner().getUuid()));
			to.setCreated(from.getCreated());
			to.setUuid(from.getUuid());
		}
	}

	private void copyEntity(Follow from, Follow to) {
		if ((from != null) && (to != null)) {
			to.setList(getLinklistByUuid(from.getList().getUuid()));
			to.setUser(getUserByUuid(from.getUser().getUuid()));
			to.setUuid(from.getUuid());
		}
	}
	
	private User getUserByUuid(UUID uuid) {
		User entity = entityManager
				.createQuery("select u from users u where uuid = :uuid",
						User.class).setParameter("uuid", uuid)
				.getSingleResult();

		return entity;
	}

	private Linklist getLinklistByUuid(UUID uuid) {
		Linklist entity = entityManager
				.createQuery("select l from linklists l where uuid = :uuid",
						Linklist.class).setParameter("uuid", uuid)
				.getSingleResult();

		return entity;
	}

	private Link getLinkByUuid(UUID uuid) {
		Link entity = entityManager
				.createQuery("select l from links l where uuid = :uuid",
						Link.class).setParameter("uuid", uuid)
				.getSingleResult();

		return entity;
	}

	private List<Log> log;

	/**
	 * The Class Log.
	 */
	public class Log {
		private String title;
		private String message;

		/**
		 * Instantiates a new log.
		 *
		 * @param title
		 *            the title
		 * @param message
		 *            the message
		 */
		public Log(String title, String message) {
			this.title = title;
			this.message = message;
		}

		/**
		 * Gets the title.
		 *
		 * @return the title
		 */
		public String getTitle() {
			return title;
		}

		/**
		 * Gets the message.
		 *
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}
	}

	/**
	 * Gets the log.
	 *
	 * @return the log
	 */
	public List<Log> getLog() {
		return log;
	}

	/**
	 * Sets the log.
	 *
	 * @param log
	 *            the new log
	 */
	public void setLog(List<Log> log) {
		this.log = log;
	}
}
