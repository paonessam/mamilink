/*
 * 
 */
package ch.mami.link.webservice;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import ch.mami.link.controller.BaseManager;
import ch.mami.link.model.Comment;
import ch.mami.link.model.Credentials;
import ch.mami.link.model.Follow;
import ch.mami.link.model.Link;
import ch.mami.link.model.Linklist;
import ch.mami.link.model.User;
import ch.mami.link.webservice.SyncManager.Log;

// TODO: Auto-generated Javadoc
/**
 * The Class SyncClientManager.
 */
@ManagedBean(name = "syncManager")
@ViewScoped
public class SyncClientManager extends BaseManager {

	private String syncServiceUrl = "http://83.78.113.93:8080/MaMiLink/rest/sync";// "http://localhost:8080/MaMiLink/rest/sync";

	private SyncManager syncManager;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		if (currentUser == null) {
			gotoBase("login");
		} else {
			// create sync manager
			this.syncManager = new SyncManager(entityManager, utx);
		}
	}

	/**
	 * Gets the sync service url.
	 *
	 * @return the sync service url
	 */
	public String getSyncServiceUrl() {
		return syncServiceUrl;
	}

	/**
	 * Sets the sync service url.
	 *
	 * @param syncServiceUrl
	 *            the new sync service url
	 */
	public void setSyncServiceUrl(String syncServiceUrl) {
		if (syncServiceUrl.endsWith("/")) {
			this.syncServiceUrl = syncServiceUrl.substring(0,
					syncServiceUrl.length());
		} else {
			this.syncServiceUrl = syncServiceUrl;
		}
	}

	/**
	 * Gets the log.
	 *
	 * @return the log
	 */
	public List<Log> getLog() {
		return this.syncManager.getLog();
	}

	/**
	 * Sync.
	 */
	public void sync() {
		Client client = ClientBuilder.newClient();

		this.syncManager.getLog().clear();

		// sync users
		List<User> users = client.target(syncServiceUrl + "/users")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<User>>() {
				});
		users = this.syncManager.syncUsers(users);

		// sync users
		List<Credentials> credentials = client
				.target(syncServiceUrl + "/creds")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Credentials>>() {
				});
		credentials = this.syncManager.syncCredentials(credentials);

		// sync lists
		List<Linklist> lists = client.target(syncServiceUrl + "/lists")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Linklist>>() {
				});
		lists = this.syncManager.syncLinklists(lists);

		// sync links
		List<Link> links = client.target(syncServiceUrl + "/links")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Link>>() {
				});
		links = this.syncManager.syncLinks(links);

		// sync comments
		List<Comment> comments = client.target(syncServiceUrl + "/comments")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Comment>>() {
				});
		comments = this.syncManager.syncComments(comments);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		// sync comments
		List<Follow> follows = client.target(syncServiceUrl + "/follows")
				.request(MediaType.APPLICATION_JSON)
				.get(new GenericType<List<Follow>>() {
				});
		follows = this.syncManager.syncFollows(follows);

		for (Log log : syncManager.getLog()) {
			System.out.println(log.getTitle() + " --- " + log.getMessage());
		}

		// sync back to service
		client.target(syncServiceUrl + "/users")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(users, MediaType.APPLICATION_JSON),
						User.class);

		client.target(syncServiceUrl + "/creds")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(credentials, MediaType.APPLICATION_JSON),
						Credentials.class);

		client.target(syncServiceUrl + "/lists")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(lists, MediaType.APPLICATION_JSON),
						Linklist.class);

		client.target(syncServiceUrl + "/links")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(links, MediaType.APPLICATION_JSON),
						Link.class);

		client.target(syncServiceUrl + "/comments")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(comments, MediaType.APPLICATION_JSON),
						Comment.class);

		client.target(syncServiceUrl + "/follows")
				.request(MediaType.APPLICATION_JSON)
				.put(Entity.entity(follows, MediaType.APPLICATION_JSON),
						Follow.class);
	}
}
