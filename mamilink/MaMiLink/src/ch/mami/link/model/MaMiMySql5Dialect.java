/*
 * 
 */
package ch.mami.link.model;

import org.hibernate.dialect.MySQL5Dialect;

// TODO: Auto-generated Javadoc
/**
 * The Class MaMiMySql5Dialect.
 */
public class MaMiMySql5Dialect extends MySQL5Dialect {

	/**
	 * Instantiates a new ma mi my sql5 dialect.
	 */
	public MaMiMySql5Dialect(){
	      super();
	   }
	
	/* (non-Javadoc)
	 * @see org.hibernate.dialect.MySQLDialect#supportsCascadeDelete()
	 */
	@Override
	public boolean supportsCascadeDelete() {
		System.out.println("--- supportsCascadeDelete() called.");
		return true;
	}

}
