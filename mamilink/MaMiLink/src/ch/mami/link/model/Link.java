/*
 * 
 */
package ch.mami.link.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

// TODO: Auto-generated Javadoc
/**
 * The Class Link.
 */
@Entity(name = "links")
public class Link extends BaseEntity {

	private static final long serialVersionUID = 1834054302729295967L;

	@JoinColumn(referencedColumnName = "id", name = "list", foreignKey = @ForeignKey(name = "FK_links_linklists"))
	@ManyToOne(optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Linklist list;

	@Column(length = 255)
	// @Pattern(regexp =
	// "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")
	private String url;

	@Column(length = 50)
	@Pattern(regexp = "[ -0-9a-zA-Z������+&@#/%=~_|!:,.;]*")
	private String title;

	@Column(nullable = false)
	private Integer position;

	/**
	 * Instantiates a new link.
	 */
	public Link() {
	}

	/**
	 * Instantiates a new link.
	 *
	 * @param title the title
	 */
	public Link(String title) {
		super();
		setTitle(title);
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public Linklist getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(Linklist list) {
		this.list = list;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

	/**
	 * Gets the link url.
	 *
	 * @return the link url
	 */
	public String getLinkUrl() {
		if (url.contains("http://")) {
			return url;
		} else {
			return "http://" + url;
		}
	}

	/**
	 * Sets the link url.
	 *
	 * @param link the new link url
	 */
	public void setLinkUrl(String link) {
		// just for rest...
	}
}
