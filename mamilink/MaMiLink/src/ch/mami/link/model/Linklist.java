/*
 * 
 */
package ch.mami.link.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

// TODO: Auto-generated Javadoc
/**
 * The Class Linklist.
 */
@Entity(name = "linklists")
public class Linklist extends BaseEntity {

	private static final long serialVersionUID = -6487459929149042703L;

	@Column(length = 50)
	@Pattern(regexp = "[ -0-9a-zA-Z������+&@#/%=~_|!:,.;]*")
	private String title;

	@JoinColumn(referencedColumnName = "id", name = "owner", foreignKey = @ForeignKey(name = "FK_linklists_users"))
	@ManyToOne(optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User owner;

	@Column(name = "private", nullable = false)
	private Boolean privat;

	@Column(nullable = false)
	private Integer position;

	/**
	 * Instantiates a new linklist.
	 */
	public Linklist() {
	}

	/**
	 * Instantiates a new linklist.
	 *
	 * @param title the title
	 */
	public Linklist(String title) {
		super();
		setTitle(title);
	}

	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}

	/**
	 * Gets the privat.
	 *
	 * @return the privat
	 */
	public Boolean getPrivat() {
		return privat;
	}

	/**
	 * Sets the privat.
	 *
	 * @param privat the new privat
	 */
	public void setPrivat(Boolean privat) {
		this.privat = privat;
	} 

	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * Sets the position.
	 *
	 * @param position the new position
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}
}
