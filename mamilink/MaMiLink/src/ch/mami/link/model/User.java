/*
 * 
 */
package ch.mami.link.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Pattern;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
@Entity(name = "users")
public class User extends BaseEntity {

	private static final long serialVersionUID = 1599795664358608910L;

	@Column(nullable = false, length = 50)
	@Pattern(regexp = "[a-zA-Z������][a-zA-Z������-]*")
	private String name;

	@Column(nullable = true, length = 50)
	@Pattern(regexp = "[a-zA-Z������][a-zA-Z������-]*")
	private String surname;

	@Column(nullable = false, length = 255)
	@Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
			+ "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"
			+ "@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
	private String email;

	@Column(nullable = false, unique = true, length = 50)
	@Pattern(regexp = "[a-zA-Z������-]*")
	private String username;

	@Column(nullable = false)
	private Boolean admin;

	/**
	 * Instantiates a new user.
	 */
	public User() {
		super();
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param username the username
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param admin the admin
	 */
	public User(String username, String name, String surname, String email,
			Boolean admin) {
		super();
		this.username = username;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.admin = admin;
	}

	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the admin.
	 *
	 * @return the admin
	 */
	public Boolean getAdmin() {
		return admin;
	}

	/**
	 * Sets the admin.
	 *
	 * @param admin the new admin
	 */
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}

}