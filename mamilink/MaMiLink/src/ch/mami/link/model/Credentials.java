/*
 * 
 */
package ch.mami.link.model;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

// TODO: Auto-generated Javadoc
/**
 * The Class Credentials.
 */
@Entity(name = "credentials")
public class Credentials extends BaseEntity {

	private static final long serialVersionUID = -3212744979910943908L;

	@Column
	private String username;

	@Column(name = "password")
	private String password;

	/**
	 * Sets the password.
	 *
	 * @param password
	 *            the new encrypted password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the encrypted password.
	 *
	 * @return the encrypted password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Encrypts and sets the clear-text-password.
	 *
	 * @param passwordClear
	 *            the new clear-text-password
	 * @throws NoSuchAlgorithmException
	 *             the no such algorithm exception
	 */
	public void setPasswordWithEncryption(String passwordClear)
			throws NoSuchAlgorithmException {

		MessageDigest md = MessageDigest.getInstance("MD5");
		md.reset();
		byte[] decryptedpasswordbyte = passwordClear.getBytes();
		byte[] digested = md.digest(decryptedpasswordbyte);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < digested.length; i++) {
			sb.append(Integer.toHexString(0xff & digested[i]));
		}
		this.password = sb.toString();
	}

	@JoinColumn(referencedColumnName = "id", name = "user", foreignKey = @ForeignKey(name = "FK_credentials_users"))
	@OneToOne(optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username
	 *            the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
