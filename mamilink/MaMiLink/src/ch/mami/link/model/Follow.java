/*
 * 
 */
package ch.mami.link.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

// TODO: Auto-generated Javadoc
/**
 * The Class Follow.
 */
@Entity(name = "follows")
public class Follow extends BaseEntity {

	private static final long serialVersionUID = 405568398297524864L;
	
	@JoinColumn(referencedColumnName = "id", name = "user", foreignKey = @ForeignKey(name = "FK_follows_users"))
	@ManyToOne(optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE) 
	private User user;
	
	@JoinColumn(referencedColumnName = "id", name = "list", foreignKey = @ForeignKey(name = "FK_follows_linklists"))
	@ManyToOne(optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Linklist list;
 
	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public Linklist getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(Linklist list) {
		this.list = list;
	}

}
