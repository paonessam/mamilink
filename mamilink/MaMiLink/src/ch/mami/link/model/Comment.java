/*
 * 
 */
package ch.mami.link.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

// TODO: Auto-generated Javadoc
/**
 * The Class Comment.
 */
@Entity(name = "comments") 
public class Comment extends BaseEntity {

	private static final long serialVersionUID = -8807407424842923010L;

	@Column(length = 50)
	//@Pattern(regexp = "[ -0-9a-zA-Z������+&@#/%=~_|!:,.;]*")
	private String title;

	@Column(length = 160)
	//@Pattern(regexp = "[ -0-9a-zA-Z������+&@#/%=~_|!:,.;]*")
	private String message;

	@JoinColumn(referencedColumnName = "id", name = "owner", foreignKey = @ForeignKey(name = "FK_comments_users"))
	@ManyToOne(optional = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User owner;

	@JoinColumn(referencedColumnName = "id", name = "list", foreignKey = @ForeignKey(name = "FK_comments_linklists"))
	@ManyToOne(optional = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Linklist list;

	@JoinColumn(referencedColumnName = "id", name = "link", foreignKey = @ForeignKey(name = "FK_comments_links"))
	@ManyToOne(optional = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Link link;

	@Column(nullable = false) 
	private Boolean deleted;
	
	@Column(nullable = false)
	private Timestamp created; 

	/**
	 * Instantiates a new comment.
	 */
	public Comment() {
		this.deleted = false;
	}
	
	/**
	 * Gets the title.
	 *
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title.
	 *
	 * @param title the new title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	public User getOwner() {
		return owner;
	}

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	public void setOwner(User owner) {
		this.owner = owner;
	}

	/**
	 * Gets the list.
	 *
	 * @return the list
	 */
	public Linklist getList() {
		return list;
	}

	/**
	 * Sets the list.
	 *
	 * @param list the new list
	 */
	public void setList(Linklist list) {
		this.list = list;
	}

	/**
	 * Gets the link.
	 *
	 * @return the link
	 */
	public Link getLink() {
		return link;
	}

	/**
	 * Sets the link.
	 *
	 * @param link the new link
	 */
	public void setLink(Link link) {
		this.link = link;
	}

	/**
	 * Gets the deleted.
	 *
	 * @return the deleted
	 */
	public Boolean getDeleted() {
		return deleted;
	}

	/**
	 * Sets the deleted.
	 *
	 * @param deleted the new deleted
	 */
	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Gets the created.
	 *
	 * @return the created
	 */
	public Timestamp getCreated() {
		return created;
	}

	/**
	 * Sets the created.
	 *
	 * @param created the new created
	 */
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	
}
