/*
 * 
 */
package ch.mami.link.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseEntity.
 */
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 3862029257154894246L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;

	@Column(nullable = false, unique = true)
	@Type(type = "uuid-char")
	private UUID uuid;

	@Column(nullable = false)
	private Timestamp lastChange;

	@Transient
	private Boolean editable;
	
	/**
	 * Instantiates a new base entity.
	 */
	public BaseEntity() {
		this.editable = false;
		this.uuid = UUID.randomUUID();
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public UUID getUuid() {
		return uuid;
	}

	/**
	 * Sets the uuid.
	 *
	 * @param uuid the new uuid
	 */
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	/**
	 * Gets the last change.
	 *
	 * @return the last change
	 */
	public Timestamp getLastChange() {
		return lastChange;
	} 

	/**
	 * Sets the last change.
	 *
	 * @param lastChange the new last change
	 */
	public void setLastChange(Timestamp lastChange) {
		this.lastChange = lastChange;
	}

	/**
	 * Gets the editable.
	 *
	 * @return the editable
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * Sets the editable.
	 *
	 * @param editable the new editable
	 */
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof BaseEntity) {
			BaseEntity be = (BaseEntity) obj;
			if (this.uuid.compareTo(be.getUuid()) == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
