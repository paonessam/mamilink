/*
 * 
 */
package ch.mami.link.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

// TODO: Auto-generated Javadoc
/**
 * The Class CredentialsManager.
 */
@Named
@RequestScoped
public class CredentialsManager {
	
	private String username;

    private String password;
    
	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * Sets the password.
	 *
	 * @param decryptedpassword the new password
	 * @throws NoSuchAlgorithmException the no such algorithm exception
	 */
	public void setPassword(String decryptedpassword) throws NoSuchAlgorithmException {
    	if (password == null) {
        	
        	MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            byte[] decryptedpasswordbyte = decryptedpassword.getBytes();
            byte[] digested = md.digest(decryptedpasswordbyte);
            StringBuffer sb = new StringBuffer();
            for(int i=0;i<digested.length;i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            
            this.password = sb.toString();
        }
    }
}
