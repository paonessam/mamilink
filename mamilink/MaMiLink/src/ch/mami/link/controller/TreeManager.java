/*
 * 
 */
package ch.mami.link.controller;

import java.util.Comparator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import ch.mami.link.model.Link;
import ch.mami.link.model.Linklist;
import ch.mami.link.tree.LinkNode;
import ch.mami.link.tree.LinklistNode;

// TODO: Auto-generated Javadoc
/**
 * The Class TreeManager.
 */
@ManagedBean(name = "treeManager")
@ViewScoped
public class TreeManager extends BaseManager implements Serializable {

	private static final long serialVersionUID = -1310195020134804283L;

	private TreeNode root;
	private TreeNode selectedNode;

	/**
	 * Gets the root.
	 *
	 * @return the root
	 */
	public TreeNode getRoot() {
		return root;
	}

	/**
	 * Sets the root.
	 *
	 * @param root the new root
	 */
	public void setRoot(DefaultTreeNode root) {
		this.root = root;
	}

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
		if (currentUser == null) {
			gotoBase("login");
		} else {
			loadTree();
		}
	}

	/**
	 * The Class LinkNodeComparator.
	 */
	class LinkNodeComparator implements Comparator<TreeNode> {

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(TreeNode arg0, TreeNode arg1) {
			if ((arg0.getData() instanceof LinkNode)
					&& (arg0.getData() instanceof LinkNode)) {
				Link first = ((LinkNode) arg0.getData()).getLink();
				Link second = ((LinkNode) arg1.getData()).getLink();
				Integer comp = first.getPosition().compareTo(
						second.getPosition());
				if (comp == 0) {
					return first.getTitle().compareTo(second.getTitle());
				} else {
					return comp;
				}
			}
			return 0;
		}

	}

	/**
	 * Gets the selected node.
	 *
	 * @return the selected node
	 */
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	/**
	 * Sets the selected node.
	 *
	 * @param selectedNode the new selected node
	 */
	public void setSelectedNode(TreeNode selectedNode) {
		if (this.selectedNode != null) {
			this.selectedNode.setSelected(false);
		}
		this.selectedNode = selectedNode;
		if (this.selectedNode != null) {
			this.selectedNode.setSelected(true);
		}
	}

	/**
	 * On node select.
	 *
	 * @param event the event
	 */
	public void onNodeSelect(NodeSelectEvent event) {
		setSelectedNode(event.getTreeNode());
	}

	/**
	 * On drag drop.
	 *
	 * @param event the event
	 */
	public void onDragDrop(TreeDragDropEvent event) {
		TreeNode dragNode = event.getDragNode();
		TreeNode dropNode = event.getDropNode();

		FacesMessage message = null;
		if (dropNode.getType() == "linklist") {
			LinklistNode list = (LinklistNode) dropNode.getData();

			if (!list.getLinklist().getOwner().equals(currentUser)) {
				message = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"forbidden action",
						"your are not allowed to change a followed list!");
			} else if (dragNode.getType() == "link") {
				dragNode.setParent(dropNode);

				Link link = ((LinkNode) dragNode.getData()).getLink();
				link.setList(list.getLinklist());
				this.selectedNode = dragNode;
				// save link
				String msg = "";
				msg = super.save(link);
				if (msg == "") {
					link.setEditable(false);
				} else {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage(msg));
				}
				// set new positions
				resetPositions(dropNode);
			}
		}
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	private void resetPositions(TreeNode dropNode) {
		Integer pos = 0;
		// all links updated in order of tree
		for (TreeNode node : dropNode.getChildren()) {
			Link link = ((LinkNode) node.getData()).getLink();
			link.setPosition(pos);
			super.save(link);
			pos++;
		}
	}

	/**
	 * Adds the linklist.
	 *
	 * @param newTitle the new title
	 */
	public void addLinklist(String newTitle) {
		RequestContext context = RequestContext.getCurrentInstance();
		Boolean bOk = false;

		Linklist list = new Linklist(newTitle);
		list.setOwner(currentUser);
		list.setPrivat(true);
		list.setPosition(root.getChildCount() + 1);
		super.save(list);

		LinklistNode listNode = new LinklistNode(list);
		DefaultTreeNode treeNode = new DefaultTreeNode("linklist", listNode,
				root);
		treeNode.setExpanded(true);

		setSelectedNode(treeNode);

		bOk = true;
		
		context.addCallbackParam("added", bOk);
	}

	/**
	 * Adds the link.
	 *
	 * @param linklistNode the linklist node
	 * @param newTitle the new title
	 * @param newUrl the new url
	 */
	public void addLink(DefaultTreeNode linklistNode, String newTitle,
			String newUrl) {
		RequestContext context = RequestContext.getCurrentInstance();
		Boolean bOk = false;

		Linklist list = ((LinklistNode) linklistNode.getData()).getLinklist();
		if (!list.getOwner().equals(currentUser)) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO,
							"forbidden action",
							"your are not allowed to change a followed list!"));
		} else {
			Link link = new Link(newTitle);
			link.setUrl(newUrl);
			link.setList(list);
			link.setPosition(linklistNode.getChildCount() + 1);
			super.save(link);
			LinkNode linkNode = new LinkNode(link);
			DefaultTreeNode TreeNode = new DefaultTreeNode("link", linkNode,
					linklistNode);
			setSelectedNode(TreeNode);
			bOk = true;
		}
		context.addCallbackParam("added", bOk);
	}

	/**
	 * Delete current.
	 */
	public void deleteCurrent() {
		FacesMessage message = null;

		if (this.selectedNode.getType() == "link") {
			LinkNode linkNode = (LinkNode) this.selectedNode.getData();

			if (!linkNode.getLink().getList().getOwner().equals(currentUser)) {
				message = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"forbidden action",
						"your are not allowed to change a followed list!");
			} else {
				this.selectedNode.getParent().getChildren()
						.remove(this.selectedNode);
				super.delete(linkNode.getLink());
				this.selectedNode = null;
			}
		} else if (this.selectedNode.getType() == "linklist") {
			LinklistNode linklistNode = (LinklistNode) this.selectedNode
					.getData();

			if (!linklistNode.getLinklist().getOwner().equals(currentUser)) {
				message = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"forbidden action",
						"your are not allowed to change a followed list!");
			} else {
				if (this.selectedNode.getChildCount() == 0) {
					root.getChildren().remove(this.selectedNode);
					if (linklistNode.getLinklist().getId() != null) {
						super.delete(linklistNode.getLinklist());
					}
					this.selectedNode = null;
				} else {
					message = new FacesMessage(
							"List can't be deleted, because it has childs!");
				}
			}
		}
		if (message != null) {
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	/**
	 * Save current.
	 */
	public void saveCurrent() {
		if (this.selectedNode.getType() == "link") {
			Link link = ((LinkNode) this.selectedNode.getData()).getLink();
			String msg = "";
			msg = super.save(link);
			if (msg == "") {
				link.setEditable(false);
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(msg));
			}
		} else if (this.selectedNode.getType() == "linklist") {
			Linklist linklist = ((LinklistNode) this.selectedNode.getData())
					.getLinklist();
			String msg = "";
			msg = super.save(linklist);
			if (msg == "") {
				linklist.setEditable(false);
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage(msg));
			}
		}
	}

	/**
	 * Cancel current.
	 */
	public void cancelCurrent() {
		if (this.selectedNode.getType() == "link") {
			LinkNode linkNode = (LinkNode) this.selectedNode.getData();

			if (linkNode.getLink().getId() == null) {
				this.selectedNode.getParent().getChildren()
						.remove(this.selectedNode);
				this.selectedNode = null;
			} else {
				Integer id = linkNode.getLink().getId();
				try {
					Link oldLink = entityManager
							.createQuery("from links where id = :id",
									Link.class).setParameter("id", id)
							.getSingleResult();
					if (oldLink != null) {
						linkNode.setLink(oldLink);
					}
				} catch (NoResultException nre) {
					// ignore
				}
			}
		} else if (this.selectedNode.getType() == "linklist") {
			LinklistNode linklistNode = (LinklistNode) this.selectedNode
					.getData();
			if (linklistNode.getLinklist().getId() == null) {
				root.getChildren().remove(this.selectedNode);
				this.selectedNode = null;
			} else {
				Integer id = linklistNode.getLinklist().getId();
				try {
					Linklist oldLinklist = entityManager
							.createQuery("from linklists where id = :id",
									Linklist.class).setParameter("id", id)
							.getSingleResult();
					if (oldLinklist != null) {
						linklistNode.setLinklist(oldLinklist);
					}
				} catch (NoResultException nre) {
					// ignore
				}
			}
		}
	}

	/**
	 * Edits the current.
	 */
	public void editCurrent() {
		if (this.selectedNode.getType() == "link") {
			((LinkNode) this.selectedNode.getData()).setEditable(true);
		} else if (this.selectedNode.getType() == "linklist") {
			((LinklistNode) this.selectedNode.getData()).setEditable(true);
		}
	}

	/**
	 * Load tree.
	 */
	public void loadTree() {
		root = new DefaultTreeNode("Root", null);

		List<Linklist> linklists = entityManager
				.createQuery("select l from linklists l where owner = :owner",
						Linklist.class).setParameter("owner", currentUser)
				.getResultList();

		// if no records in db then the list is null
		if ((linklists == null) || (linklists.isEmpty())) {
			System.out.println("linklist = null");
			linklists = new ArrayList<Linklist>();

			Linklist list = new Linklist();
			list.setTitle("Default list");
			list.setOwner(currentUser);
			list.setPrivat(true);
			list.setPosition(0);
			super.save(list);

			linklists.add(list);
		}

		List<Linklist> fLinklists = entityManager
				.createQuery(
						"select l from linklists l, follows f where l.id = f.list and f.user = :user",
						Linklist.class).setParameter("user", currentUser)
				.getResultList();

		if (fLinklists != null) {
			linklists.addAll(fLinklists);
		}

		for (Linklist linklist : linklists) {
			DefaultTreeNode listNode = new DefaultTreeNode("linklist",
					new LinklistNode(linklist), root);
			listNode.setExpanded(true);

			List<Link> links = entityManager
					.createQuery(
							"select l from links l where l.list = :list order by l.position",
							Link.class).setParameter("list", linklist)
					.getResultList();
			for (Link link : links) {
				new DefaultTreeNode("link", new LinkNode(link), listNode);
			}
			listNode.getChildren().sort(new LinkNodeComparator());
		}
	}
}
