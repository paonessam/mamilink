/*
 * 
 */
package ch.mami.link.controller;

import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;

import org.primefaces.context.RequestContext;

import ch.mami.link.model.Credentials;
import ch.mami.link.model.User;

/**
 * The Class AdminManager.
 */
@ManagedBean(name = "adminManager")
@ViewScoped
public class AdminManager extends BaseManager implements Serializable {

	private static final long serialVersionUID = -2294340103412290879L;

	// private Boolean isAdmin = false;
	private List<User> users;
	private User user;

	/**
	 * Inits the AdminManager.
	 */
	@PostConstruct
	public void init() {
		if (currentUser == null) {
			gotoBase("login");
		} else {
			loadUsers();
		}
	}

	/**
	 * Checks if is admin.
	 *
	 * @return the boolean
	 */
	public Boolean isAdmin() {
		return currentUser.getAdmin();
	}

	/**
	 * Gets the users.
	 *
	 * @return the users
	 */
	public List<User> getUsers() {
		return users;
	}

	/**
	 * Sets the users.
	 *
	 * @param users
	 *            the new users
	 */
	public void setUsers(List<User> users) {
		this.users = users;
	}

	/**
	 * Gets the user.
	 *
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * Sets the user.
	 *
	 * @param user
	 *            the new user
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * Load users.
	 */
	public void loadUsers() {
		// load userlist just for admin-users
		if (currentUser.getAdmin()) {
			users = entityManager.createQuery(
					"select u from users u order by username", User.class)
					.getResultList();
		}
		// load user always
		user = entityManager
				.createQuery("select u from users u where id = :id", User.class)
				.setParameter("id", currentUser.getId()).getSingleResult();
	}

	/**
	 * Delete.
	 *
	 * @param user
	 *            the user
	 */
	public void delete(User user) {
		if (currentUser.getAdmin()) {
			users.remove(user);
			super.delete(user);
		}
	}

	/**
	 * Save.
	 *
	 * @param user
	 *            the user
	 */
	public void save(User user) {
		String msg = "";
		msg = super.save(user);
		if (msg == "") {
			user.setEditable(false);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(msg));
		}
	}

	/**
	 * Cancel.
	 *
	 * @param user
	 *            the user
	 */
	public void cancel(User user) {
		user.setEditable(false);
		if (user.getId() == null) {
			users.remove(user);
		} else {
			Integer id = user.getId();
			try {
				user = entityManager
						.createQuery("from users where id = :id", User.class)
						.setParameter("id", id).getSingleResult();
			} catch (NoResultException nre) {
				// ignore
			}
		}
	}

	/**
	 * Edits the.
	 *
	 * @param user
	 *            the user
	 */
	public void edit(User user) {
		user.setEditable(true);
	}

	/**
	 * Adds a new user.
	 *
	 * @param username
	 *            the username
	 * @param name
	 *            the name
	 * @param surname
	 *            the surname
	 * @param email
	 *            the email
	 * @param admin
	 *            the admin
	 */
	public void add(String username, String name, String surname, String email,
			String admin) {
		if (currentUser.getAdmin()) {
			RequestContext context = RequestContext.getCurrentInstance();
			Boolean bOk = false;
			String msg = "";

			if (!username.isEmpty() && !name.isEmpty() && !email.isEmpty()) {
				if (entityManager
						.createQuery(
								"select count(u) from users u where username = :username",
								Long.class).setParameter("username", username)
						.getSingleResult() == 0) {

					Boolean isAdmin = Boolean.parseBoolean(admin);
					User user = new User(username, name, surname, email,
							isAdmin);
					user.setEditable(true);
					msg = super.save(user);
					if (msg.isEmpty()) {
						users.add(user);
						bOk = true;
					} else {
						FacesContext.getCurrentInstance().addMessage(null,
								new FacesMessage(msg));
					}
				} else {
					FacesContext.getCurrentInstance().addMessage(null,
							new FacesMessage("username already exists!"));
				}
			}
			context.addCallbackParam("added", bOk);
		}
	}

	public void changePassword(User user, String newPassword) {
		System.out.println("change password");
		RequestContext context = RequestContext.getCurrentInstance();

		if ((!newPassword.isEmpty()) && (user != null)
				&& (user.equals(currentUser) || currentUser.getAdmin())) {

			Credentials creds = entityManager
					.createQuery(
							"select c from credentials c where user = :user",
							Credentials.class).setParameter("user", user)
					.getSingleResult();

			String msg = null;
			try {
				creds.setPasswordWithEncryption(newPassword);
				msg = super.save(creds);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				msg = e.getMessage();
			} catch (Exception e) {
				msg = e.getMessage();
			}

			if (msg.isEmpty()) {
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_INFO,
								"password changed!",
								"successfully changed the password"));
				context.addCallbackParam("changed", true);
				context.addCallbackParam("message", "");
			} else {
				context.addCallbackParam("changed", false);
				context.addCallbackParam("message", msg);
			}
		} else {
			context.addCallbackParam("changed", false);
			context.addCallbackParam("message", "can't change that password!");
		}
	}
}