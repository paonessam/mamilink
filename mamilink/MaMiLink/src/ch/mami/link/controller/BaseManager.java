/*
 * 
 */
package ch.mami.link.controller;

import java.io.IOException;
import java.sql.Timestamp;

import javax.annotation.Resource;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.UserTransaction;

import ch.mami.link.model.BaseEntity;
import ch.mami.link.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class BaseManager.
 */
public abstract class BaseManager {

	@PersistenceContext(unitName = "MaMiLink")
	protected EntityManager entityManager;

	@Resource
	protected UserTransaction utx;
	
	@ManagedProperty(value="#{loginManager.currentUser}")
	protected User currentUser;

	/**
	 * Gets the current user.
	 *
	 * @return the current user
	 */
	public User getCurrentUser() {
		return currentUser;
	}

	/**
	 * Sets the current user.
	 *
	 * @param currentUser the new current user
	 */
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	/**
	 * Save.
	 *
	 * @param obj the obj
	 * @return the string
	 */
	public String save(BaseEntity obj) {
		obj.setLastChange(new Timestamp(System.currentTimeMillis()));
		if (obj.getId() == null) {
			return insert(obj);
		} else {
			return update(obj);
		}
	}

	/**
	 * Delete.
	 *
	 * @param obj the obj
	 */
	public void delete(BaseEntity obj) {
		if (obj.getId() != null) {
			try {
				utx.begin();
				entityManager.remove(entityManager.contains(obj) ? obj
						: entityManager.merge(obj));
				utx.commit();
			} catch (Exception e) {
				try {
					utx.rollback();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				e.printStackTrace();
			}
		}
	}

	private String insert(Object obj) {
		try {
			utx.begin();
			entityManager.persist(obj);
			utx.commit();
			return "";
		} catch (javax.transaction.RollbackException re) {
			if ((re.getCause() instanceof PersistenceException)
					&& (re.getCause().getCause().getClass().toString()
							.contains("ConstraintViolationException"))) {
				if (re.getCause().getCause().getCause().getMessage()
						.contains("UNQ_EMAIL")) {
					return "unique constaint violation: email allready exists!";
				} else {
					return "unique constaint violation";
				}
			} else {
				return re.getCause().toString();
			}
		} catch (Exception e) {
			return "Saving failed! Error: " + e.getMessage();
		}
	}

	private String update(Object obj) {
		try {
			utx.begin();
			entityManager.merge(obj);
			utx.commit();
			return "";
		} catch (javax.transaction.RollbackException re) {
			if ((re.getCause() instanceof PersistenceException)
					&& (re.getCause().getCause().getClass().toString()
							.contains("ConstraintViolationException"))) {
				if (re.getCause().getCause().getCause().getMessage()
						.contains("UNQ_EMAIL")) {
					return "unique constaint violation: email allready exists!";
				} else {
					return "unique constaint violation";
				}
			} else {
				return re.getCause().toString();
			}
		} catch (Exception e) {
			return "Saving failed! Error: " + e.getMessage();
		}
	}
	
	/**
	 * Goto base.
	 *
	 * @param page the page
	 */
	public void gotoBase(String page) {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			externalContext.redirect(page.concat(".xhtml"));
			System.out.println("redirect to ".concat(page));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
