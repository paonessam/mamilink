/*
 * 
 */
package ch.mami.link.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import ch.mami.link.model.Credentials;
import ch.mami.link.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class LoginManager.
 */
@ManagedBean(name = "loginManager")
@SessionScoped
public class LoginManager implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private CredentialsManager credentials;
	private Credentials actualcredentials;

	@PersistenceContext(unitName = "MaMiLink")
	private EntityManager entityManager;

	private User currentUser;

	/**
	 * Login.
	 */
	public void login() {
		boolean loggedIn = isLoggedIn();

		System.out.println("Logged:" + isLoggedIn());
		if (currentUser != null) {
			System.out.println("Loggedbefore:" + currentUser.getUsername());
		}
		System.out.println("PW " + credentials.getPassword());
		System.out.println("User " + credentials.getUsername());

		if (!loggedIn) {
			
			try {

			actualcredentials = entityManager
					.createQuery(
							"select c from credentials c where username = :username and password = :password)",
							Credentials.class)
					.setParameter("username", credentials.getUsername())
					.setParameter("password", credentials.getPassword())
					.getSingleResult();
			
			} catch (NoResultException e) {
			
				actualcredentials = null;
				FacesContext.getCurrentInstance().addMessage(null,new FacesMessage("Wrong Credentials!"));
				
			}

			if (actualcredentials != null) {

				currentUser = actualcredentials.getUser();
				System.out.println(currentUser.getName());
				gotoBase("links");

			} else {
				loggedIn = false;
			}

			loggedIn = isLoggedIn();
			
		} else {

			try {
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("Loggedout");
		}
		System.out.println("Logged:" + isLoggedIn());
	}

	/**
	 * Logout.
	 */
	public void logout() {

		currentUser = null;
		System.out.println(currentUser);
		
		try {
			FacesContext.getCurrentInstance().getViewRoot().getViewMap()
					.remove("adminManager");
			FacesContext.getCurrentInstance().getViewRoot().getViewMap()
					.remove("treeManager");
			FacesContext.getCurrentInstance().getViewRoot().getViewMap()
					.remove("commentManager");
			FacesContext.getCurrentInstance().getViewRoot().getViewMap()
					.remove("followManager");
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	/**
	 * Checks if is logged in.
	 *
	 * @return true, if is logged in
	 */
	public boolean isLoggedIn() {

		return currentUser != null;

	}

	/**
	 * Sets the current user.
	 *
	 * @param currentUser the new current user
	 */
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	/**
	 * Gets the current user.
	 *
	 * @return the current user
	 */
	public User getCurrentUser() {
		return this.currentUser;
	}

	/**
	 * Goto base.
	 *
	 * @param page the page
	 */
	public void gotoBase(String page) {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			externalContext.redirect(page.concat(".xhtml"));
			System.out.println("redirect to ".concat(page));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}