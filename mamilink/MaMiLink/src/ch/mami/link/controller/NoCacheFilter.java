/*
 * 
 */
package ch.mami.link.controller;

import java.io.IOException;

import javax.faces.application.ResourceHandler;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// TODO: Auto-generated Javadoc
/**
 * The Class NoCacheFilter.
 */
//@WebFilter(servletNames={"Faces Servlet"})
public class NoCacheFilter implements Filter {
	  
	private FilterConfig filterConfig = null;
	  
	  /* (non-Javadoc)
  	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
  	 */
  	public void init(FilterConfig filterConfig) {
	    this.filterConfig = filterConfig;
	  }
	  
	  /* (non-Javadoc)
  	 * @see javax.servlet.Filter#destroy()
  	 */
  	public void destroy() {
	    this.filterConfig = null;
	  }

	  /*The real work happens in doFilter(). 
	  The reference to the response object is of type ServletResponse,
	  so we need to cast it to HttpServletResponse:
	  */

	  /* (non-Javadoc)
  	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
  	 */
  	public void doFilter(ServletRequest request, ServletResponse response,
	      FilterChain chain)
	      throws IOException, ServletException {
	    HttpServletResponse httpResponse = (HttpServletResponse) response;

	    /*Then we just set the appropriate headers
	    and invoke the next filter in the chain:
	    */
	    httpResponse.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
	    httpResponse.setDateHeader("Expires", -1);
	    httpResponse.setHeader("Pragma", "No-cache");
	    chain.doFilter(request, response);
	    /* this method calls other filters in the order they are 
	    written in web.xml
	    */
	    
	    System.out.println(chain.toString());
	    
	    
	  }
	}