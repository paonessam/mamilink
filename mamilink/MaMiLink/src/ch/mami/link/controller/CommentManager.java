/*
 * 
 */
package ch.mami.link.controller;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import ch.mami.link.model.Comment;
import ch.mami.link.model.Link;
import ch.mami.link.model.Linklist;
import ch.mami.link.tree.LinkNode;
import ch.mami.link.tree.LinklistNode;

// TODO: Auto-generated Javadoc
/**
 * The Class CommentManager.
 */
@ManagedBean(name = "commentManager")
@ViewScoped
public class CommentManager extends BaseManager implements Serializable {

	private static final long serialVersionUID = -2294340103412290879L;

	private List<Comment> comments;
	private TreeNode currentNode;
	
	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {		
		
	}

	/**
	 * Gets the comments.
	 *
	 * @return the comments
	 */
	public List<Comment> getComments() {
		return comments;
	}

	/**
	 * Sets the comments.
	 *
	 * @param comments the new comments
	 */
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	/**
	 * Gets the comments.
	 *
	 * @param node the node
	 * @return the comments
	 */
	public List<Comment> getComments(TreeNode node) {
		if ((currentNode == null)
				|| (currentNode.getType() != node.getType())
				|| ((node.getType() == "linklist") && (((LinklistNode) node
						.getData()).compareTo((LinklistNode) currentNode
						.getData())) != 0)
				|| ((node.getType() == "link") && (((LinkNode) node.getData())
						.compareTo((LinkNode) currentNode.getData())) != 0)) {
			currentNode = node;
			loadComments(node);
		}
		return comments;
	}

	private void loadComments(TreeNode node) {
		if (comments != null) {
			comments.clear();
		}

		if (node.getType() == "link") {
			Link link = ((LinkNode) node.getData()).getLink();
			if (link.getId() != null) {
				comments = entityManager
						.createQuery(
								"select c from comments c where link = :link order by c.created",
								Comment.class).setParameter("link", link)
						.getResultList();
			}
		} else if (node.getType() == "linklist") {
			Linklist linklist = ((LinklistNode) node.getData()).getLinklist();
			if (linklist.getId() != null) {
				comments = entityManager
						.createQuery(
								"select c from comments c where list = :list order by c.created",
								Comment.class).setParameter("list", linklist)
						.getResultList();
			}
		}

		if (comments == null) {
			comments = new ArrayList<Comment>();
		}
	}

	/**
	 * Adds the comment.
	 *
	 * @param node the node
	 * @param newTitle the new title
	 * @param newMessage the new message
	 */
	public void addComment(DefaultTreeNode node, String newTitle, String newMessage) {
		Link link = null;
		Linklist list = null;
		Boolean bOk = false;
		RequestContext context = RequestContext.getCurrentInstance();
		
		if (!newTitle.isEmpty() && !newMessage.isEmpty()) {
			if (node.getType() == "link") {
				link = ((LinkNode) node.getData()).getLink();
			} else if (node.getType() == "linklist") {
				list = ((LinklistNode) node.getData()).getLinklist();
			}
			if ((link != null) || (list != null)) {
				Comment comment = new Comment();
				comment.setTitle(newTitle);
				comment.setMessage(newMessage);
				comment.setLink(link);
				comment.setList(list);
				comment.setOwner(currentUser);
				comment.setCreated(new Timestamp(System.currentTimeMillis()));
				comments.add(comment);

				String msg = "";
				msg = super.save(comment);
				bOk = (msg == "");
			}
		}
		context.addCallbackParam("added", bOk);
	}

	/**
	 * Delete comment.
	 *
	 * @param comment the comment
	 */
	public void deleteComment(Comment comment) {
		if (comment.getId() == null) {
			comments.remove(comment);
		} else {
			comment.setTitle("deleted by owner");
			comment.setMessage("");
			comment.setDeleted(true);
			super.save(comment);
		}
	}
}
