/*
 * 
 */
package ch.mami.link.controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import ch.mami.link.model.Credentials;
import ch.mami.link.model.User;

// TODO: Auto-generated Javadoc
/**
 * The Class RegisterManager.
 */
@ManagedBean(name = "userBean")
@ViewScoped
public class RegisterManager extends BaseManager {

	private User user;

	private Credentials credentials;

	@NotNull
	@NotEmpty
	// @Pattern(regexp = "[A-Z][a-zA-Z������-]*")
	private String name;

	@NotNull
	@NotEmpty
	@Pattern(regexp = "[A-Z][a-zA-Z������-]*")
	private String surname;

	@NotNull
	@NotEmpty
	@Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
			+ "[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*"
			+ "@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
	private String email;

	@Pattern(regexp = "[a-zA-Z������-]*")
	private String username;

	private String password;

	@Transient
	private Boolean editable;

	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Register.
	 */
	public void register() {

		user = new User(username, name, surname, email, false);
		System.out.println("UserAdded");

		credentials = new Credentials();
		try {
			credentials.setPasswordWithEncryption(password);
		} catch (NoSuchAlgorithmException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		credentials.setUsername(username);
		credentials.setUser(user);

		try {

			if (credentials.getId() == null) {

				super.save(user);
				super.save(credentials);

			} else {
				// fentityManager.merge(user);
			}

			System.out.println("save commited");
			gotoBase("login");
			
			

		} catch (Exception e) {
			// return "Saving failed! Error: " + e.getMessage();
		}

	}
	
	/**
	 * Goto base.
	 *
	 * @param page the page
	 */
	public void gotoBase(String page) {
		ExternalContext externalContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		try {
			externalContext.redirect(page.concat(".xhtml"));
			System.out.println("redirect to ".concat(page));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
