/*
 * 
 */
package ch.mami.link.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import ch.mami.link.model.Follow;
import ch.mami.link.model.Linklist;

// TODO: Auto-generated Javadoc
/**
 * The Class FollowManager.
 */
@ManagedBean(name = "followManager")
@ViewScoped
public class FollowManager extends BaseManager implements Serializable {

	private static final long serialVersionUID = 1111502574690505612L;

	private String searchText;
	private List<Linklist> searchResults;

	/**
	 * Inits the.
	 */
	@PostConstruct
	public void init() {
	}

	/**
	 * Gets the search text.
	 *
	 * @return the search text
	 */
	public String getSearchText() {
		return searchText;
	}

	/**
	 * Sets the search text.
	 *
	 * @param searchText the new search text
	 */
	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	/**
	 * Gets the search results.
	 *
	 * @return the search results
	 */
	public List<Linklist> getSearchResults() {
		return searchResults;
	}

	/**
	 * Sets the search results.
	 *
	 * @param searchResults the new search results
	 */
	public void setSearchResults(List<Linklist> searchResults) {
		this.searchResults = searchResults;
	}

	/**
	 * Search.
	 */
	public void search() {
		searchResults = entityManager
				.createQuery(
						"select l from linklists l where l.owner != :user and l.title like :search and not exists (select f from follows f where l.owner = f.user and l.id = f.list) order by l.title",
						Linklist.class)
				.setParameter("search", "%" + searchText + "%")
				.setParameter("user", currentUser).getResultList();
	}

	/**
	 * Adds the follow.
	 *
	 * @param linklist the linklist
	 */
	public void addFollow(Linklist linklist) {

		RequestContext context = RequestContext.getCurrentInstance();
		Boolean bOk = false;

		if (linklist != null) {
			String msg = "";

			if (entityManager
					.createQuery(
							"select count(f) from follows f where list = :list and user = :user",
							Long.class).setParameter("list", linklist)
					.setParameter("user", currentUser).getSingleResult() > 0) {
				// already following
				msg = "you already follow this list";
			} else {
				Follow follow = new Follow();
				follow.setList(linklist);
				follow.setUser(currentUser);
				msg = super.save(follow);
				if (msg.isEmpty()) {
					msg = "now you follow this list";
					bOk = true;
				}
			}
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(msg));
			System.out.println(msg);
		}

		context.addCallbackParam("added", bOk);
		
		if (bOk) {
			gotoBase("links");
		}
	}

	/**
	 * Delete follow.
	 *
	 * @param linklist the linklist
	 */
	public void deleteFollow(Linklist linklist) {
		Follow follow = entityManager
				.createQuery(
						"select f from follows f where list = :list and user = :user",
						Follow.class).setParameter("list", linklist)
				.setParameter("user", currentUser).getSingleResult();

		deleteFollow(follow);
		
		gotoBase("links");
	}

	/**
	 * Delete follow.
	 *
	 * @param follow the follow
	 */
	public void deleteFollow(Follow follow) {
		if (follow != null) {
			super.delete(follow);
		}
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage("now you don't follow this list anymore"));
	}

}
