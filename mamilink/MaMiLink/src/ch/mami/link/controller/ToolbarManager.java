/*
 * 
 */
package ch.mami.link.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;

// TODO: Auto-generated Javadoc
/**
 * The Class ToolbarManager.
 */
@ManagedBean(name = "toolbarManager")
@ViewScoped
public class ToolbarManager extends BaseManager implements Serializable {

	private static final long serialVersionUID = -6706415316590709450L;

	/**
	 * Gets the checks if is logged in.
	 *
	 * @return the checks if is logged in
	 */
	public Boolean getIsLoggedIn() {
		return (currentUser != null);
	}

	/**
	 * Logout.
	 */
	public void logout() {
		if (currentUser != null) {
			FacesContext context = FacesContext.getCurrentInstance();
			LoginManager loginmanager = context.getApplication().evaluateExpressionGet(context, "#{loginManager}", LoginManager.class);
			loginmanager.logout();
			gotoBase("login");
		}
	}

	/**
	 * Goto register.
	 */
	public void gotoRegister() {
		if (currentUser == null) {
			gotoBase("register");
		}
	}

	/**
	 * Goto admin.
	 */
	public void gotoAdmin() {
		if ((currentUser != null) && (currentUser.getAdmin())) {
			gotoBase("admin");
		}
	}

	/**
	 * Goto links.
	 */
	public void gotoLinks() {
		if (currentUser != null) {
			gotoBase("links");
		}
	}

	/**
	 * Goto login.
	 */
	public void gotoLogin() {
		if (currentUser == null) {
			gotoBase("login");
		}
	}

	/**
	 * Goto profile.
	 */
	public void gotoProfile() {
		if (currentUser != null) {
			gotoBase("profile");
		}
	}

	/**
	 * Goto sync.
	 */
	public void gotoSync() {
		if ((currentUser != null) && (currentUser.getAdmin())) {
			gotoBase("sync");
		}
	}
}
