/*
 * 
 */
package ch.mami.link.tree;

// TODO: Auto-generated Javadoc
/**
 * The Interface BaseNode.
 */
public interface BaseNode {

	/**
	 * Gets the type.
	 *
	 * @return the type
	 */
	abstract public String getType();

	/**
	 * Gets the editable.
	 *
	 * @return the editable
	 */
	abstract public Boolean getEditable();

	/**
	 * Sets the editable.
	 *
	 * @param active the new editable
	 */
	abstract public void setEditable(Boolean active);
	
}
