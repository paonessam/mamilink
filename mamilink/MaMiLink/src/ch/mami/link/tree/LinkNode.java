/*
 * 
 */
package ch.mami.link.tree;

import java.io.Serializable;

import ch.mami.link.model.Link;

// TODO: Auto-generated Javadoc
/**
 * The Class LinkNode.
 */
public class LinkNode implements BaseNode, Serializable, Comparable<LinkNode> {

	private static final long serialVersionUID = -2721633993075210490L;

	private Link link;

	/**
	 * Instantiates a new link node.
	 *
	 * @param link the link
	 */
	public LinkNode(Link link) {
		this.setLink(link);
	}

	/**
	 * Gets the link.
	 *
	 * @return the link
	 */
	public Link getLink() {
		return link;
	}

	/**
	 * Sets the link.
	 *
	 * @param link the new link
	 */
	public void setLink(Link link) {
		this.link = link;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if (link != null)
			return link.getTitle();
		else
			return super.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(LinkNode node) {
		return this.link.getUuid().compareTo(node.getLink().getUuid());
	}

	/* (non-Javadoc)
	 * @see ch.mami.link.tree.BaseNode#getEditable()
	 */
	@Override
	public Boolean getEditable() {
		return this.link.getEditable();
	}

	/* (non-Javadoc)
	 * @see ch.mami.link.tree.BaseNode#setEditable(java.lang.Boolean)
	 */
	@Override
	public void setEditable(Boolean active) {
		this.link.setEditable(active);
	}

	/* (non-Javadoc)
	 * @see ch.mami.link.tree.BaseNode#getType()
	 */
	@Override
	public String getType() {
		return "link";
	}

}
