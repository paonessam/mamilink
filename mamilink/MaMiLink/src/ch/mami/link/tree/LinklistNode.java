/*
 * 
 */
package ch.mami.link.tree;

import java.io.Serializable;

import ch.mami.link.model.Linklist;

// TODO: Auto-generated Javadoc
/**
 * The Class LinklistNode.
 */
public class LinklistNode implements BaseNode, Serializable,
		Comparable<LinklistNode> {

	private static final long serialVersionUID = -6500330607084542220L;

	private Linklist linklist;
	private Boolean expanded;

	/**
	 * Instantiates a new linklist node.
	 *
	 * @param linklist the linklist
	 */
	public LinklistNode(Linklist linklist) {
		this.linklist = linklist;
	}

	/**
	 * Gets the linklist.
	 *
	 * @return the linklist
	 */
	public Linklist getLinklist() {
		return linklist;
	}

	/**
	 * Sets the linklist.
	 *
	 * @param linklist the new linklist
	 */
	public void setLinklist(Linklist linklist) {
		this.linklist = linklist;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return this.linklist.getTitle();
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(LinklistNode linklistNode) {
		return this.linklist.getUuid().compareTo(linklistNode.getLinklist().getUuid());
	}

	/* (non-Javadoc)
	 * @see ch.mami.link.tree.BaseNode#getEditable()
	 */
	@Override
	public Boolean getEditable() {
		return this.linklist.getEditable();
	}

	/* (non-Javadoc)
	 * @see ch.mami.link.tree.BaseNode#setEditable(java.lang.Boolean)
	 */
	@Override
	public void setEditable(Boolean active) {
		this.linklist.setEditable(active);
	}

	/* (non-Javadoc)
	 * @see ch.mami.link.tree.BaseNode#getType()
	 */
	@Override
	public String getType() {
		return "linklist";
	}

	/**
	 * Gets the expanded.
	 *
	 * @return the expanded
	 */
	public Boolean getExpanded() {
		return this.expanded;
	}

	/**
	 * Sets the expanded.
	 *
	 * @param expanded the new expanded
	 */
	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}
}
