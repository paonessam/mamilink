USE MaMiLink;

delete from follows;
delete from comments;
delete from links;
delete from linklists;
delete from users;

insert into users (username, surname, name, email, admin, uuid, lastchange) values ('mf', 'Furrer', 'Michael', 'mickyf@sunrise.ch', 1, UUID(), now());
insert into users (username, surname, name, email, admin, uuid, lastchange) values ('mp', 'Paonessa', 'Mario', 'mario.paonessa@bluewin.ch', 0, UUID(), now());
insert into users (username, surname, name, email, admin, uuid, lastchange) values ('gb', 'Br�smeli', 'Gusti', 'gusti@broesmeli.ch', 0, UUID(), now());

insert into linklists (position, private, title, owner, uuid, lastchange) 
select users.id - 1, 0, 'Suchmaschinen', users.id, UUID(), now() from users;
insert into linklists (position, private, title, owner, uuid, lastchange) 
select users.id - 1, 0, 'Landkarten', users.id, UUID(), now() from users;
insert into linklists (position, private, title, owner, uuid, lastchange) 
select users.id - 1, 0, 'Videos', users.id, UUID(), now() from users;

insert into links (position, title, url, list, uuid, lastchange) 
select 0, 'Google', 'www.google.ch', linklists.id, UUID(), now() from linklists where title = 'Suchmaschinen';
insert into links (position, title, url, list, uuid, lastchange)
select 1, 'Bing', 'www.bing.ch', linklists.id, UUID(), now() from linklists where title = 'Suchmaschinen';
insert into links (position, title, url, list, uuid, lastchange)
select 2, 'Search', 'www.search.ch', linklists.id, UUID(), now() from linklists where title = 'Suchmaschinen';

insert into links (position, title, url, list, uuid, lastchange) 
select 0, 'GoogleMaps', 'maps.google.ch', linklists.id, UUID(), now() from linklists where title = 'Landkarten';
insert into links (position, title, url, list, uuid, lastchange)
select 1, 'Bing Maps', 'maps.bing.ch', linklists.id, UUID(), now() from linklists where title = 'Landkarten';
insert into links (position, title, url, list, uuid, lastchange)
select 2, 'Map-Search', 'map.search.ch', linklists.id, UUID(), now() from linklists where title = 'Landkarten';

insert into links (position, title, url, list, uuid, lastchange)
select 0, 'Youtube', 'www.youtube.com', linklists.id, UUID(), now() from linklists where title = 'Videos';

insert into follows (user, list, uuid, lastchange) values (1, 6, UUID(), now());