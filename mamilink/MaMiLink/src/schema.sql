
    create table comments (
        id integer not null auto_increment,
        lastChange datetime not null,
        uuid varchar(255) not null,
        created datetime not null,
        deleted bit not null,
        message varchar(160),
        title varchar(50),
        link integer,
        list integer,
        owner integer not null,
        primary key (id)
    );

    create table credentials (
        id integer not null auto_increment,
        lastChange datetime not null,
        uuid varchar(255) not null,
        password varchar(255),
        username varchar(255),
        user integer not null,
        primary key (id)
    );

    create table follows (
        id integer not null auto_increment,
        lastChange datetime not null,
        uuid varchar(255) not null,
        list integer not null,
        user integer not null,
        primary key (id)
    );

    create table linklists (
        id integer not null auto_increment,
        lastChange datetime not null,
        uuid varchar(255) not null,
        position integer not null,
        private bit not null,
        title varchar(50),
        owner integer not null,
        primary key (id)
    );

    create table links (
        id integer not null auto_increment,
        lastChange datetime not null,
        uuid varchar(255) not null,
        position integer not null,
        title varchar(50),
        url varchar(255),
        list integer not null,
        primary key (id)
    );

    create table users (
        id integer not null auto_increment,
        lastChange datetime not null,
        uuid varchar(255) not null,
        admin bit not null,
        email varchar(255) not null,
        name varchar(50) not null,
        surname varchar(50),
        username varchar(50) not null,
        primary key (id)
    );

    alter table comments 
        add constraint UK_1ng1qxaquhwjvlhkkxkhohhs1  unique (uuid);

    alter table credentials 
        add constraint UK_kgwbx5avtuw6xu51sf1ei3xfw  unique (user);

    alter table credentials 
        add constraint UK_gjwsag0kdvjdt1vk1l92hggqc  unique (uuid);

    alter table follows 
        add constraint UK_kynshct1dddt2o05ybsxxarnc  unique (uuid);

    alter table linklists 
        add constraint UK_93xgqnbj37tenvmgs3qbl2p22  unique (uuid);

    alter table links 
        add constraint UK_11xdogr9prm1u06uifxtqycdl  unique (uuid);

    alter table users 
        add constraint UK_6km2m9i3vjuy36rnvkgj1l61s  unique (uuid);

    alter table users 
        add constraint UK_r43af9ap4edm43mmtq01oddj6  unique (username);

    alter table comments 
        add constraint FK_comments_links 
        foreign key (link) 
        references links (id) 
        on delete cascade;

    alter table comments 
        add constraint FK_comments_linklists 
        foreign key (list) 
        references linklists (id) 
        on delete cascade;

    alter table comments 
        add constraint FK_comments_users 
        foreign key (owner) 
        references users (id) 
        on delete cascade;

    alter table credentials 
        add constraint FK_credentials_users 
        foreign key (user) 
        references users (id) 
        on delete cascade;

    alter table follows 
        add constraint FK_follows_linklists 
        foreign key (list) 
        references linklists (id) 
        on delete cascade;

    alter table follows 
        add constraint FK_follows_users 
        foreign key (user) 
        references users (id) 
        on delete cascade;

    alter table linklists 
        add constraint FK_linklists_users 
        foreign key (owner) 
        references users (id) 
        on delete cascade;

    alter table links 
        add constraint FK_links_linklists 
        foreign key (list) 
        references linklists (id) 
        on delete cascade;
