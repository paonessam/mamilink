package ch.mami.me.model;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.UserTransaction;

@ManagedBean(name = "userManager", eager = true)
@SessionScoped
public class UserManager {
	@PersistenceContext(unitName = "MiniEntity")
	private EntityManager entityManager;

	@Resource
	private UserTransaction utx;

	// private User user;
	private List<User> users;

	@PostConstruct
	public void init() {
		users = entityManager.createQuery("from User", User.class)
				.getResultList();

		// if no records in db then the list is null
		if (users == null) {
			users = new ArrayList<User>();
		}
	}

	public List<User> getUsers() {
		return users;
	}

	public void addUser() {
		User user = new User("Name", "Vorname", "email@dres.se");
		users.add(user);
		user.setEditable(true);
	}

	public void saveUser(User user) {
		String msg = save(user);
		if (msg == "") {
			user.setEditable(false);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(msg));
		}
	}

	public void cancelUser(User user) {
		if (user.getId() == null) {
			users.remove(user);
		} else {
			Integer id = user.getId();
			Integer index = users.indexOf(user);
			users.remove(user);
			User oldUser = null;
			try {
				oldUser = entityManager
						.createQuery("from User where id = :id", User.class)
						.setParameter("id", id).getSingleResult();
			} catch (NoResultException nre) {
				// ignore
			}

			if (oldUser != null) {
				users.add(index, oldUser);
			}
		}
	}

	public void editUser(User user) {
		user.setEditable(true);
	}

	public void deleteUser(User user) {
		users.remove(user);
		delete(user);
	}

	// just for debug
	public void printList() {
		for (User user : users) {
			System.out.println("User (" + user.getId().toString() + ", "
					+ user.getName() + ")");
		}
	}

	private String save(User user) {
		try {
			utx.begin();
			if (user.getId() == null) {
				entityManager.persist(user);
			} else {
				entityManager.merge(user);
			}
			utx.commit();
			System.out.println("save commited");
			return "";
		} catch (javax.transaction.RollbackException re) {
			if ((re.getCause() instanceof PersistenceException)
					&& (re.getCause().getCause().getClass().toString()
							.contains("ConstraintViolationException"))) {
				if (re.getCause().getCause().getCause().getMessage()
						.contains("UNQ_EMAIL")) {
					return "unique constaint violation: email allready exists!";
				} else {
					return "unique constaint violation";
				}
			} else {
				return re.getCause().toString();
			}
		} catch (Exception e) {
			return "Saving failed! Error: " + e.getMessage();
		}
	}

	private void delete(User user) {
		try {
			utx.begin();
			entityManager.remove(entityManager.contains(user) ? user
					: entityManager.merge(user));
			utx.commit();
			System.out.println("save commited!" + utx.getStatus());
		} catch (Exception e) {
			try {
				utx.rollback();
			} catch (Exception e2) {
				e2.printStackTrace();
			}
			e.printStackTrace();
		}
	}
}