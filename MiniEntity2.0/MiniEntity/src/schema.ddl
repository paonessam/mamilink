
    create table User (
        id integer not null auto_increment,
        email varchar(254),
        name varchar(32),
        surname varchar(32),
        primary key (id)
    );

    alter table User 
        add constraint UNQ_EMAIL  unique (email);
